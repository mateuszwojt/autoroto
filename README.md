# autoRoto

Very crude ML-based rotoscoping application based on ResNET-101. It takes images as input, lets you select a category of the segmentation and creates a black-and-white matte as output.



This application has the following dependencies:
- Python 3.9.x
- pytorch
- PySide6
- PIL
- [filetype](https://pypi.org/project/filetype/)

## Installation

1. Install Python virtual environment

```bash
pip install virtualenv
```

2. Clone the repository and go into the directory

```
git clone https://gitlab.com/mateuszwojt/autoroto.git
cd autoroto
```

3. Create a new virtual environment from the project requirements

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

4. Run the application

```
python -m main
```

