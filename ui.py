import os

from PySide6 import QtWidgets, QtGui, QtCore

from impl import process_file
from utils import create_output_directory, list_input_files

MATTE_HEIGHT_DEFAULT = 256


class Worker(QtCore.QThread):
    updateProgress = QtCore.Signal(int)

    input_directory = None
    output_directory = None
    matte_height = None
    categories = {}
    files = []

    def __init__(self):
        QtCore.QThread.__init__(self)

    def run(self):
        total_count = len(self.files)

        for idx, f in enumerate(self.files):
            process_file(os.path.join(self.input_directory, f),
                         self.output_directory,
                         int(self.matte_height),
                         self.categories)

            # ensure progress bar reaches 100%
            idx += 1
            progress_val = int(100/total_count*idx)
            self.updateProgress.emit(progress_val)


class MainWindow(QtWidgets.QDialog):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("AutoRoto")
        self.setGeometry(0, 0, 300, 480)

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setSpacing(20)
        self.setLayout(self.layout)

        self.worker = Worker()

        self.create_ui()
        self.connect_ui()

    def create_ui(self):
        self.select_images_btn = QtWidgets.QPushButton("Select Input Directory")
        self.layout.addWidget(self.select_images_btn)

        matte_height_grp = QtWidgets.QGroupBox()
        matte_height_grp_lyt = QtWidgets.QVBoxLayout()
        matte_height_grp.setTitle("Set Matte Height:")
        matte_height_grp.setLayout(matte_height_grp_lyt)

        self.matte_height = QtWidgets.QLineEdit(str(MATTE_HEIGHT_DEFAULT))
        int_validator = QtGui.QIntValidator()
        self.matte_height.setValidator(int_validator)
        self.set_matte_height_btn = QtWidgets.QPushButton("Set")
        matte_height_grp_lyt.addWidget(self.matte_height)
        matte_height_grp_lyt.addWidget(self.set_matte_height_btn)
        self.layout.addWidget(matte_height_grp)

        matte_category_grp = QtWidgets.QGroupBox()
        matte_category_grp_lyt = QtWidgets.QVBoxLayout()
        matte_category_grp.setTitle("Create Matte For:")
        matte_category_grp.setLayout(matte_category_grp_lyt)

        self.people_chk = QtWidgets.QCheckBox("People")
        self.people_chk.setChecked(True)
        self.vehicles_chk = QtWidgets.QCheckBox("Vehicles")
        self.animals_chk = QtWidgets.QCheckBox("Animals")
        self.household_chk = QtWidgets.QCheckBox("Household Objects")
        matte_category_grp_lyt.addWidget(self.people_chk)
        matte_category_grp_lyt.addWidget(self.vehicles_chk)
        matte_category_grp_lyt.addWidget(self.animals_chk)
        matte_category_grp_lyt.addWidget(self.household_chk)
        self.layout.addWidget(matte_category_grp)

        self.start_btn = QtWidgets.QPushButton("Start Rotoscoping")
        self.layout.addWidget(self.start_btn)

        self.progress_bar = QtWidgets.QProgressBar()
        self.progress_bar.setMinimum(0)
        self.progress_bar.setMaximum(100)
        self.layout.addWidget(self.progress_bar)

    def connect_ui(self):
        self.worker.updateProgress.connect(self.set_progress)
        self.select_images_btn.clicked.connect(self.get_images_path)
        self.start_btn.clicked.connect(self.run)

    def get_images_path(self):
        selected_directory = QtWidgets.QFileDialog.getExistingDirectory()
        if selected_directory:
            self.worker.input_directory = selected_directory
            self.worker.files = list_input_files(selected_directory)
            self.worker.output_directory = create_output_directory(selected_directory)

    def get_selected_categories(self):
        return {
            "people": self.people_chk.isChecked(),
            "vehicles": self.vehicles_chk.isChecked(),
            "animals": self.animals_chk.isChecked(),
            "other": self.household_chk.isChecked()
        }

    def run(self):
        if not self.worker.output_directory:
            self.show_message("Input/output directory is not set")
            raise Exception("Input/output directory is not set")

        if not self.worker.files:
            self.show_message("No image files found in selected directory")
            raise Exception("No image files found in selected directory")

        self.worker.matte_height = int(self.matte_height.text())
        self.worker.categories = self.get_selected_categories()
        self.worker.start()

    def set_progress(self, value):
        self.progress_bar.setValue(value)

    def reset_progress(self):
        self.progress_bar.setValue(0)

    def show_message(self, msg):
        QtWidgets.QMessageBox.warning(None, "Error", msg, QtWidgets.QMessageBox.Ok)
