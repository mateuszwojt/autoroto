import os
import shutil
import filetype

OUTPUT_DIR_NAME = "Output"


def create_output_directory(dir_path: str):
    output_dir_path = os.path.join(dir_path, OUTPUT_DIR_NAME)
    if os.path.isdir(output_dir_path):
        # remove the output directory and recreate an empty one
        shutil.rmtree(output_dir_path)
        os.mkdir(output_dir_path)
    else:
        # directory doesn't exist yet, create one
        os.mkdir(output_dir_path)

    return output_dir_path


def list_input_files(dir_path: str):
    files = [f for f in os.listdir(dir_path) if os.path.isfile(os.path.join(dir_path, f))]
    image_files = [f for f in files if filetype.is_image(os.path.join(dir_path, f))]
    return image_files
