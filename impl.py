import os.path

import numpy as np
import torch
import torchvision.transforms as transforms
from torchvision import models
from PIL import Image


def get_roto_model():
    fcn = models.segmentation.fcn_resnet101(pretrained=True).eval()
    if torch.cuda.is_available():
        fcn.cuda()
    return fcn


def process_file(path: str, output_dir: str, matte_size: int, mapping: dict):
    filename = os.path.basename(path)
    name, ext = os.path.splitext(filename)
    new_name = "{}_matte{}".format(name, ext)
    output_path = os.path.join(output_dir, new_name)
    create_matte(path, output_path, matte_size, mapping)


def create_matte(filename: str, matte_name: str, matte_size: int, mapping: dict):
    img = Image.open(filename)
    w, h = img.size

    t = transforms.Compose(
        [transforms.Resize(matte_size),
         transforms.ToTensor(),
         transforms.Normalize(mean=[0.485, 0.456, 0.406],
                              std=[0.229, 0.224, 0.225])])

    i = t(img).unsqueeze(0)
    with torch.no_grad():
        fcn = get_roto_model()
        out = fcn(i)['out']
        om = torch.argmax(out.squeeze(), dim=0).detach().cpu().numpy()
        rgb = decode_segmap(om, mapping=mapping)
        im = Image.fromarray(rgb)
        im = im.resize((w, h))
        im.save(matte_name)


def decode_segmap(image: np.array, nc=21, mapping=None):
    mapping = mapping or {
        "people": True,
        "vehicles": False,
        "animals": False,
        "other": False
    }
    people = 255 if mapping["people"] else 0
    vehicles = 255 if mapping["vehicles"] else 0
    animals = 255 if mapping["animals"] else 0
    other = 255 if mapping["other"] else 0
    
    label_colors = np.array([(0, 0, 0),  # 0=background
                             # 1=aeroplane, 2=bicycle, 3=bird, 4=boat, 5=bottle
                             (vehicles, vehicles, vehicles),
                             (vehicles, vehicles, vehicles), (animals, animals, animals),
                             (vehicles, vehicles, vehicles), (other, other, other),
                             # 6=bus, 7=car, 8=cat, 9=chair, 10=cow
                             (vehicles, vehicles, vehicles),
                             (vehicles, vehicles, vehicles), (animals, animals, animals),
                             (other, other, other), (animals, animals, animals),
                             # 11=dining table, 12=dog, 13=horse, 14=motorbike, 15=person
                             (other, other, other), (animals, animals, animals),
                             (animals, animals, animals), (vehicles, vehicles, vehicles),
                             (people, people, people),
                             # 16=potted plant, 17=sheep, 18=sofa, 19=train, 20=tv/monitor
                             (other, other, other), (animals, animals, animals),
                             (other, other, other), (vehicles, vehicles, vehicles),
                             (other, other, other)])

    r = np.zeros_like(image).astype(np.uint8)
    g = np.zeros_like(image).astype(np.uint8)
    b = np.zeros_like(image).astype(np.uint8)

    for l in range(0, nc):
        idx = image == l
        r[idx] = label_colors[l, 0]
        g[idx] = label_colors[l, 1]
        b[idx] = label_colors[l, 2]

    rgb = np.stack([r, g, b], axis=2)
    return rgb
